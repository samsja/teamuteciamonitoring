import time
from port import *
from GestionConfig import *
from tkinter.messagebox import *


fichier = open("data.txt", "w")
lecture = open("Arduino_Data.txt","r")
t = time.time()

def InitGestionTrame():
        global t
        t = time.time()

def SetFichierENRG(nomFichier,fenetre,titre,mode):
        global fichier
        fichier = open(nomFichier, mode)
        fenetre.title(titre+" ("+nomFichier+")" )

def LireDePuisFichier(fichier):
        global lecture
        lecture.close()
        lecture = open(fichier,"r")


def getNewTrameLecture():
        time.sleep(0.1)
        trame = lecture.readline()
        trame = trame[0:len(trame) - 1]
        #return "100\t15.95\t65.93\t25.12\t0\t0\t0\t1.15\t0\t0"

        return trame

def getNewTrame(modeLecture):
        if modeLecture == 'capteur':
                return getNewTramePortCom()
        else :
                return getNewTrameLecture()


def getNewTrameBis():
        trame = lecture.readline()
        trame = trame[0:len(trame) - 2]
        #return "100\t15.95\t65.93\t25.12\t0\t0\t0\t1.15\t0\t0"
        tnew = time.time()
        delta = tnew -t
        print(delta)
        if ( tnew -t > 0.1):
                t = tnew
                return trame
        else:
                return ''



def save(trame):
        #print(trame)
        fichier.write(trame+'\n')
        return 0

def UpdateVariableMoteur(variableMoteur,newTrame,pause):
        if not(pause) :
                save(newTrame)

        newTrame = newTrame.split("\t")
        n= 0
        for key in variableMoteur:
                variableMoteur[key] = float(newTrame[n])
                n = n +1


def TrameEnd():
        fichier.close()
        lecture.close()

def EnrClose():
        fichier.close()




def CapteurIsOkay():
        retour = True
        if not(retour):
                showerror( "Erreur",getConfig("MESSAGE_ERREUR_CO") )
                a = 0
        return retour
