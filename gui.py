from tkinter import *
from tkinter import filedialog
from tkinter.messagebox import *
from Ressource import *
from threading import Thread
from GestionTrame import *
from math import cos
from math import sin
import shutil
from GestionConfig import *
import OptionMenu as opt

#fonction


def UpdateAffichageVariable(event):
    global variableMoteur

    for key in listeCompteur:
        listeLabel[key].setValeur(variableMoteur[key],getConfig("TEMPS_RETARD_ANIMATION(MS)"))

    for key in ListeAfficheur:
        listeLabel[key].setValeur(variableMoteur[key])


def RunFunction( pause):
    newTrame = getNewTrame(modeLecture)
    while newTrame == '':
        newTrame = getNewTrame(modeLecture)

    #print(newTrame)
    UpdateVariableMoteur(variableMoteur,newTrame,pause)
    UpdateAffichageVariable(0)

def Pause(arg):
    global main
    global buttonPause
    capteurOkay = CapteurPret()
    if(modeLecture == 'capteur' and not(capteurOkay )):
        showerror( "Erreur",getConfig("MESSAGE_ERREUR_PORT_COM") )
    elif fichierEnCours != '':
        main.pause = not(main.pause)
        if( main.BIGPause ):
            main.BIGPause = False
        if ( main.pause):
            buttonPause.config(image = PAUSE)
        else:
            buttonPause.config(image = ON)
    elif fichierEnCours == '':
        showerror( "Erreur",getConfig("MESSAGE_ERREUR_START") )



def NouveauFichier():
    global fichierEnCours
    fichierEnCours = "/temp/data.txt"
    SetFichierENRG(fichierEnCours,fenetre,getConfig("Titre"),"w")
    global buttonPause

    buttonPause.config(image = START,width = START.width(),height = START.height(),command = lambda : Pause(0) )

def OuvrirFichier():
    global fichierEnCours
    fichierEnCours = filedialog.askopenfilename(title="Ouvrir le fichier:",multiple=False, filetypes = [("Mesure","*.msr"),("All", "*")],initialdir=os.getcwd()+"/Mesures")
    if fichierEnCours != '':
        SetFichierENRG(fichierEnCours,fenetre,getConfig("Titre"),'a')
        buttonPause.config(image = START,width = START.width(),height = START.height(),command = lambda : Pause(0) )

def EnregistrerFichier():
    if fichierEnCours == '':
        showerror( "Erreur", getConfig("MESSAGE_ERREUR_ENREGISTREMENT") )
    else:
        fichierToSave = filedialog.asksaveasfilename(title="Enregistrer le fichier:",filetypes = [("Mesure","*.msr"),("All", "*")],initialdir=os.getcwd()+"/Mesures")
        if fichierToSave != '':
            EnrClose()
            split = fichierToSave.split(".")
            if len(split )> 0:
                extention = split[-1]
                if extention != "msr":
                    fichierToSave = split[0] + ".msr"
            else :
                fichierToSave = fichierToSave+".msr"

            shutil.copyfile(fichierEnCours, fichierToSave)
            SetFichierENRG(fichierToSave,fenetre,getConfig("Titre"),'a')

def RelectureFichier():
    file  = filedialog.askopenfilename(title="Ouvrir le fichier:",multiple=False,filetypes = [("Mesure","*.msr"),("All", "*")],initialdir=os.getcwd()+"/Mesures")

    if file != '':
        LireDePuisFichier(file)
        global modeLecture
        modeLecture = 'fichier'

def instructionQuitter():
    main.stop()
    TrameEnd()


def Quitter():

    if ( fichierEnCours in [ '',"data.txt"]):
        if askokcancel("quitter",getConfig("MESSAGE_QUITTER")) :
            main.BIGPause = True
            fenetre.destroy()

    else:
        main.BIGPause = True
        fenetre.destroy()



def ResetCO():
    modeLecture = 'capteur'

    #showerror( getConfig("TEXT_RESET_CONNECTION"),"Pas encore entierement codé")
    print(getConfig("Theme"))

    if(getConfig("Theme") !="Defaut"):
        setTheme("Defaut")
    else:
        setTheme("Grand")



    print(getConfig("Theme"))

    for key in listeCompteur:
        listeLabel[key].resetConfig()

    for key in ListeAfficheur:
        listeLabel[key].resetConfig()

    Pack()



def SelectCOM():
    fenetreSecondaire.lancer(opt.FenetreSelectionCOM())
    print("lancer")


def switchBl():
    global bl
    bl = not(bl)
    setBl(bl)

    if(bl):
        buttonBl.config(text = getConfig("BUTTON_BL") + " ON")
    else:
        buttonBl.config(text = getConfig("BUTTON_BL") + " OFF")



#Class

class ThreadLoop(Thread):
    def __init__(self,fonctionRun):
        Thread.__init__(self)  # ne pas oublier cette ligne
        self.running = True
        self.pause = True
        self.BIGPause = True
        self.fRun = fonctionRun



    def run(self):
        while self.running:
##            if(not(self.pause)):
##                RunFunction(self.pause)
            if(not(self.pause)):
                self.fRun(self.pause)

    def stop(self):
        self.running = False

class ThreadSolo(Thread):
    def __init__(self):
        Thread.__init__(self)  # ne pas oublier cette ligne

    def lancer(self,fRun):
            fRun()



class Compteur(Canvas):
    def __init__(self,img,hauteur,largeur,valInit,centreY,rayon,max,min,angleMin,parent,unite):
        super().__init__(parent)
        self.configure(bg='white',height=hauteur,width=largeur, background='white')
        self.image =  PhotoImage(file="Themes/" + Theme + "/"+ img)
        self.item = self.create_image(0,0,anchor=NW, image=self.image)
        self.val = valInit
        self.rayon = rayon
        self.centre = [largeur/2,centreY]
        self.max = max
        self.min = min
        self.angleMin = angleMin
        self.aig = self.create_line(0,0,0,0)
        self.theta = 0
        self.label = Label(self,text = str(valInit),fg = 'black',bg='white')
        self.label.config(font=('Helvetica',-20))
        self.label.pack()
        self.unite = unite

        self.create_window(largeur/2, hauteur -25, window=self.label)
        self.listeAig = [self.aig]

    def setExactValeur(self,val):
        self.theta = (val-self.min)/(self.max-self.min) * (360 - self.angleMin) + (val-self.max)/(self.min-self.max) * self.angleMin
        self.theta = 270 - self.theta
        self.delete(fenetre,self.aig)
        self.aig = self.create_line(self.centre[0],self.centre[1],self.centre[0] + self.rayon*cos(self.theta*22/7/180),self.centre[1] -self.rayon*sin(self.theta*22/7/180), width = 4)
        self.listeAig.append(self.aig)



    def setValeur(self,val,tmpRetard):

        n = 5
        pasTemp = 20 #ms
        oldVal = self.val
        self.val = val
        pas = (val-oldVal)/5

        for aig in self.listeAig:
            self.delete(fenetre,aig)
        self.listeAig = []


        self.setExactValeur(oldVal + pas )
        fenetre.after(pasTemp,lambda : self.setExactValeur(oldVal + 2*pas))
        fenetre.after(2*pasTemp,lambda : self.setExactValeur(oldVal + 3*pas))
        fenetre.after(3*pasTemp,lambda : self.setExactValeur(oldVal + 4*pas))
        fenetre.after(4*pasTemp,lambda : self.setExactValeur(oldVal + 5*pas))


        self.label.config(text = str(self.val)+ '  ' + self.unite)


class CompteurFromConfig(Compteur):
    def __init__(self,config,parent):
         self.parent = parent
         self.config = config
         Compteur.__init__(self,getConfig(config+'_IMAGE'),getConfig(config+"_HAUTEUR"),
                                 getConfig(config+"_LARGEUR"),0,
                                 getConfig(config+"_CENTRE"),
                                 getConfig(config+"_RAYON"),
                                 getConfig(config+"_MAX"),
                                 getConfig(config+"_MIN"),
                                 getConfig(config+"_ANGLEMAX"),
                                 parent, getConfig('UNITE_'+config))

    def resetConfig(self):

        self.delete('all')
        self.__init__(self.config,self.parent)





#Info Affichage

configInit()
setPortCom(getConfig("PORT_COM"))

Theme = getConfig("Theme")




#Ressources Moteur
variableMoteur = GetListeVariableMoteur()





#Init Tkinter
fenetre = Tk()
fenetre.title(getConfig("Titre")+" (pas de fichier selectionné)")
fenetre.config(bg="white",background='white')

#Ressources GUI
main = ThreadLoop(RunFunction)
fenetreSecondaire = ThreadSolo()
ON = PhotoImage(file="Themes/" + Theme + "/"+ "ON.png")
PAUSE = PhotoImage(file="Themes/" + Theme + "/"+ "PAUSE.png")
START = PhotoImage(file="Themes/" + Theme + "/"+ "START.png")
fichierEnCours =""

modeLecture= 'capteur' # ou 'fichier'
#modeLecture= 'fichier'

bl = False



#Widget

listeLabel = {}

ListeAfficheur = [ "thetaAir","thetaMoteur","uBatterie","angleAvance","tempsInjection","effortTraction"]

listeCompteur = ["vitesse","regimeMoteur"]


frameBasGauche = Frame(bg = "white")

frameBasDroite = Frame(bg = "white")

frameHaut = Frame(bg = "white")

frameBasMilieu = Frame(bg = "white")


for keyVar in variableMoteur:
    listeLabel[keyVar] = Label(fenetre, text=keyVar +' = NULL', bg="White")


listeLabel["vitesse"] = CompteurFromConfig("COMPTEUR_VITESSE",frameHaut)
listeLabel["regimeMoteur"] = CompteurFromConfig("COMPTEUR_REGIME",frameHaut)

listeLabel["thetaMoteur"] = AfficheurFromConfig(frameBasGauche,"TMP_MOTEUR")

listeLabel["thetaAir"] =  AfficheurFromConfig(frameBasDroite,"TMP_AIR",)


listeLabel["effortTraction"] = AfficheurFromConfig(frameBasDroite,"EFFORT_TRACTION")



listeLabel["uBatterie"] = AfficheurFromConfig(frameBasMilieu,"UBATTERIE")

listeLabel["angleAvance"] = AfficheurFromConfig(frameBasGauche,"AngleAVANCE")

listeLabel["tempsInjection"] = AfficheurFromConfig(frameBasMilieu,"TEMP_INJECTION")

#listeLabel["regimeMoteur"]=AfficheurFromConfig(frameBasDroite,"REGIME_MOTEUR")


#button

buttonPause = Button(frameHaut,text='Start',bg = "white",width=12,command= lambda :showerror( "Erreur",getConfig("MESSAGE_ERREUR_START") ))
buttonQuitter =  Button(frameHaut,text='Quit',width=12,command= Quitter,bg = "white")

buttonBl = Button(frameHaut,text='Bluetooth',width=12,command= switchBl,bg = "white")



buttonResetCO = Button(frameHaut,text=getConfig("TEXT_RESET_CONNECTION"),width=12,
                       command=ResetCO ,bg = "white")

fenetre.bind('<space>',Pause)

fenetre.bind('<Control-r>',lambda x: ResetCO())


#PACK
frameHaut.pack(side = TOP)
frameBasGauche.pack(side=LEFT)
frameBasMilieu.pack(side=LEFT)
frameBasDroite.pack(side=LEFT)

listeLabel["vitesse"].pack(side = LEFT)
listeLabel["regimeMoteur"].pack(side = RIGHT)

buttonQuitter.pack(padx = 10,pady=10)
buttonPause.pack(padx=10,pady=5)
buttonBl.pack(padx=10,pady=5)
#buttonResetCO.pack(padx=10,pady=10)


def Pack():
    for key in ListeAfficheur:
        listeLabel[key].pack()

Pack()






#Menu

menu = Menu(fenetre, background = 'white')
fenetre['menu'] = menu
sousMenuFichier = Menu(menu,background = 'white',tearoff=0)
sousMenuLecture = Menu(menu,background = 'white',tearoff=0)
sousMenuOption = Menu(menu,background = 'white',tearoff=0)
sousMenuCOM = Menu(menu,background = 'white',tearoff=0)



menu.add_cascade(label=getConfig("MENU_FICHIER"), menu=sousMenuFichier)
menu.add_cascade(label=getConfig("MENU_LECTURE"), menu = sousMenuLecture)
menu.add_cascade(label=getConfig("MENU_PORTCOM"), menu = sousMenuCOM)
menu.add_cascade(label=getConfig("MENU_OPTION"), menu = sousMenuOption)


sousMenuFichier.add_command(label=getConfig("MENU_NOUVEAU"), command= NouveauFichier)
sousMenuFichier.add_command(label=getConfig("MENU_OUVRIR"), command= OuvrirFichier)
sousMenuFichier.add_command(label=getConfig("MENU_ENR"), command= EnregistrerFichier)

sousMenuLecture.add_command(label = getConfig("MENU_RELECTURE"), command = RelectureFichier)
sousMenuCOM.add_command(label =getConfig("MENU_SELECTCOM"), command = SelectCOM)
sousMenuCOM.add_command(label =getConfig("MENU_BL"), command = switchBl)




fenetre.bind('<Control-n>',lambda x: NouveauFichier())
fenetre.bind('<Control-o>',lambda x: OuvrirFichier())
fenetre.bind('<Control-s>',lambda x: EnregistrerFichier())
fenetre.bind('<Control-l>',lambda x: RelectureFichier())
fenetre.bind('<Control-q>',lambda x: Quitter())
fenetre.bind('<Control-p>',lambda x: SelectCOM())
fenetre.bind('<Control-b>',lambda x: switchBl())


sousMenuOption.add_command(label='Preference', command= opt.option)




#Mainloop
InitGestionTrame()
main.start()
fenetre.mainloop()
instructionQuitter()
