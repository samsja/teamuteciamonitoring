

config = {}


config["TEMPS_RETARD_ANIMATION(MS)"] = 100


newElementConfig = {}

newElementConfig["Theme"] = {}
newElementConfig["preference"] = {}

miseAjourConfig = {}

miseAjourConfig["Theme"] = {}
miseAjourConfig["preference"] = {}


def configInit():
    configInitDebut()
    configInitTheme()

def configInitDebut():

    file1 = open("config.cfg" ,"r")
    line = file1.readline()
    n = 0
    while not(line[0] == 'E' and line[1] == 'N' and line[2] == 'D'):
        a = AddConfig(line)

        if a == 0:
            print("erreur dans le fichier config.cfg à la ligne " + str(n))

        line = file1.readline()
        n = n+1

    file1.close()

def configInitTheme():
    fichierCFG = open("Themes/" + config["Theme"] + "/Config/configThemes.cfg" ,"r")
    line = fichierCFG.readline()
    n = 0
    while not(line[0] == 'E' and line[1] == 'N' and line[2] == 'D'):
        a = AddConfig(line)

        if a == 0:
            print("erreur dans le fichier configTheme.cfg à la ligne " + str(n))

        line = fichierCFG.readline()
        n=n+1

    fichierCFG.close()



def getConfig(nom):
    if nom in config:
        return config[nom]
    else:
           return "erreur"


def setConfig(nom, val):
    if nom in config:
         config[nom] = val

def setTheme(theme):
    setConfig("Theme",theme)
    configInitTheme()



def AddConfig(line):
    if len(line) == 0:
        return 0
    else:
        if not(line[0] in [" ","\n","\t"]):
            #print(line)
            line = line.split("=")
            if len(line) <2:
                return 0
            if line[1][0] == '"':
                config[line[0]] = line[1][1:len(line[1])-1]
            elif line[1][0] == 'f':
                config[line[0]] = float(line[1][1:len(line[1])-1])
            else:
                config[line[0]] = int(line[1])

        return 1


def Key():
    l = list(config.keys())
    print(l)




##
##def addToNewConfig(domaine,key,val):
##    if not(domain in ["Theme","preference"]):
##        return "erreur"
##    else:
##        if key in config:
##            miseAjourConfig[domaine][key] = val
##        elif:
##            newElementConfig[domaine][key] = val
