# Module de lecture/ecriture du port série
from serial import *
from serial.tools import list_ports

port = '5'
baud = 38400

portsDispo = []

pret = True

bl = False
##port = '6'
##baud = 9600

port_serie = 0

def InitPortCom():
    return input("numero du port COM ?")

def getNewTramePortCom():
    global port
    global part_serie

    if port_serie.isOpen():
        ligne = port_serie.readline()
        global bl
        if bl:
            ligne = convTrame(ligne)
        else:
            ligne = ligne.decode()


        return ligne
    else:
        return ''

def convTrame(ligne):
    print(ligne)
    if len(ligne)>3:
        ligne = ligne.decode()
        ligne = ligne.split('-')
        ligne = "100\t0\t" +ligne[0] +"\t"+ligne[1]+"\t0\t0\t0\t1.15\t"+ligne[2]+"\t0"

    return ligne


def setPortCom(n):
    global port
    global portsDispo
    global pret
    ListPortDispo
    if ('COM' + str(n) ) in portsDispo:
        port  = str(n)
        global port_serie
        port_serie =  Serial(port='COM' + str(port), baudrate=baud, timeout=1, writeTimeout=1)
        #print( type(port_serie))
        pret = True
        return True
    else:
        pret = False
        return False



def CapteurPret():
    global pret
    return pret

def ListPortDispo():
    global portsDispo
    portsDispo = list(list_ports.comports())
    portsDispo = [ p[0] for p in portsDispo]
    return portsDispo


def setBl(bool):
    global bl
    bl = bool
