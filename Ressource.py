from tkinter import *
from tkinter import filedialog
from tkinter.messagebox import *
from GestionConfig import *


#Ressources Moteur

variableMoteur = {}

variableMoteur["tempsEcoule"] = 0   # Temps écoulé depuis la dernière trame émise [ms]
variableMoteur["vitesse"] = 0  #Vitesse proto [km/h]
variableMoteur["thetaMoteur"] = 0 # Température moteur [°c]
variableMoteur["thetaAir"] = 0 # Température de l'air [°c]
#variableMoteur["regimeMoteur"] = 0
variableMoteur["tempsInjection"] = 0 #Temps d'injection [µs]
variableMoteur["regimeMoteur"] = 0
variableMoteur["angleAvance"] = 0# Avance [°]
variableMoteur["richesse"] = 0  #Richesse [null]
variableMoteur["uBatterie"] = 0 # Tension batterie du proto [V]
variableMoteur["effortTraction"] = 0 # Effort de traction [N]



#fonction



def GetListeVariableMoteur():
    return variableMoteur



def SetVariableMoteur(str,val):
    if str in variableMoteur:
        variableMoteur[str] = val
    else:
        print(str + " varaible moteur n'existe pas '")


def SetVariableMoteur(str):
    if str in variableMoteur:
        return variableMoteur[str]
    else:
        print(str + " varaible moteur n'existe pas '")
        return "error"


class Afficheur(Frame):
    def __init__(self,parent,Titre,val, indicatrice = lambda x:0 ,unite = "" ,couleur = "White",
                 couleurDanger1 ="Orange",
                 couleurDanger2="Red",longeur = 4,taille = 30):

        Frame.__init__(self,parent)
        self.configure(bg = couleur)
        self.titre = Label(self, text=" "*longeur + Titre + " "*longeur , bg=couleur)
        self.titre.config(font=('Courrier New',-(taille//2)))
        self.val = val
        self.affVal = Label(self,text= str(self.val),bg = couleur)
        self.affVal.config(font=('Courrier New',-taille))
        self.affunite = Label(self,text= unite,bg = couleur)
        self.affunite.config(font=('Courrier New',-(taille//2)))

        self.Indicatrice = indicatrice
        self.couleur = couleur
        self.couleurDanger1 = couleurDanger1
        self.couleurDanger2 = couleurDanger2
        self.parent = parent

    def pack(self, side = 0, padx = 20, pady = 20):
        s = side
        if s == 0:
            Frame.pack(self, padx = padx ,pady = pady)
        else:
            Frame.pack(self, side = s ,padx = padx ,pady = pady)
        self.titre.pack(side = TOP)
        self.affunite.pack(side = RIGHT,padx = 20)
        self.affVal.pack(side = LEFT,padx=20)


    def grid(self,row = 0,column = 0):
        r = row
        c = column
        Frame.grid(row = r, column = c)

    def setValeur(self,val):
        self.val = val
        self.affVal.configure(text = str(self.val))

        if self.Indicatrice(val) == 0:
            couleur = self.couleur
        elif self.Indicatrice(val) == 1:
            couleur = self.couleurDanger1
        elif self.Indicatrice(val) == 2:
            couleur = self.couleurDanger2

        self.configure(bg = couleur)
        self.affVal.configure(bg = couleur)
        self.affunite.configure(bg = couleur)
        self.titre.configure(bg = couleur)




class AfficheurFromConfig(Afficheur):
    def __init__(self,parent,nom):
         croissant = getConfig(nom + '_DANGER_CROISSANT')
         self.nom = nom
         if  croissant in ["erreur",None]:
             croissant = "haut"
         elif croissant == "TRUE":
             croissant= "haut"
         else :
             croissant ="bas"

         Afficheur.__init__(self,parent,getConfig("TEXT_AFFICHEUR_"+nom),0,
                                      indicatrice = indicatrice(getConfig(nom+'_VALEUR_DANGER_1'),getConfig(nom+'_VALEUR_DANGER_2'),orientation = croissant ),
                                      unite = getConfig("UNITE_" + nom),
                                      couleur = getConfig("COULEUR_AFF_" + nom),
                                      couleurDanger1 = getConfig('COULEUR_DANGER_1_'+ nom),
                                      couleurDanger2 = getConfig('COULEUR_DANGER_2_' + nom),
                                      taille = getConfig('TAILLE_' + nom) )

    def resetConfig(self):
        deleteFrame(self)
        self.__init__(self.parent,self.nom)
        #print(getConfig('TAILLE_' + self.nom))



def deleteFrame(frame):
    for widget in frame.winfo_children():
        widget.pack_forget()

    frame.destroy()


def indicatrice( moyen, maxi, orientation = "haut"):
    if orientation == "haut":
        def f(x):
            if x < moyen:
                return 0
            elif x < maxi:
                return 1
            else:
                return 2
        return f
    else:
        def f(x):
            if x > moyen:
                return 0
            elif x > maxi:
                return 1
            else:
                return 2
        return f
